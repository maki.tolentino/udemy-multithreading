package maki.threads;

public class SampleThreads {

    public static void main(String[] args) {
        RunnerThread runner = new RunnerThread();

        // runs on main thread
        runner.run();

        // runs on a separate thread
        runner.start();

        RunnerThread runner2 = new RunnerThread();

        // runs on a separate thread
        runner2.start();

        Thread thread1 = new Thread(new RunnerRunnable());
        thread1.start();
    }

}

class RunnerThread extends Thread {
    @Override
    public void run() {
        for(int i=0; i<3; i++) {
            System.out.println("Thread: " + i);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class RunnerRunnable implements  Runnable {

    @Override
    public void run() {
        for(int i=0; i<3; i++) {
            System.out.println("Runnable: " + i);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}